CREATE TABLE IF NOT EXISTS seminars (
    Id SERIAL PRIMARY KEY,
    Begin INTEGER NOT NULL,
    "End" INTEGER NOT NULL,
    Name VARCHAR (50)
);

INSERT INTO seminars (Begin, "End", Name) VALUES
    (0, 10, 'testing data'),
    (1, 11, 'testing data 2');
