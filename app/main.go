package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

const defaultPort = "5000"
const dbPasswordFile = "/run/secrets/db-password"

var (
	port                     string
	host                     string
	databaseConnectionString string
)

func init() {
	port = os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	// it is possible to get empty string
	host = os.Getenv("HOST")

	dbUrl := os.Getenv("DATABASE")
	if dbUrl == "" {
		log.Fatalln("Database address is not specified")
	} else {
		log.Println("database url:", dbUrl)
	}

	bin, err := ioutil.ReadFile(dbPasswordFile)
	if err != nil {
		log.Fatalln(err)
	}
	dbPassword := string(bin)

	dbUser := os.Getenv("DB_USER")
	if dbUser == "" {
		dbUser = "postgres"
	}

	log.Println("user:", dbUser)
	log.Println("password:", dbPassword)

	// See: https://stackoverflow.com/questions/21959148/ssl-is-not-enabled-on-the-server
	databaseConnectionString = fmt.Sprintf("postgres://%v:%v@%v?sslmode=disable", dbUser, dbPassword, dbUrl)
}

func main() {
	db, err := NewPostgresDatabase(databaseConnectionString)
	if err != nil {
		log.Fatalln(err)
	}
	defer db.Close()
	server := NewServer(db)
	r := mux.NewRouter()
	r.HandleFunc("/list", server.HandlerList).Methods("GET")
	r.HandleFunc("/add", server.HandlerAdd).Methods("POST")
	r.Handle("/", http.RedirectHandler("/list", http.StatusTemporaryRedirect))

	log.Printf("starting server at %v:%v\n", host, port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf("%v:%v", host, port), &WithCORS{r}))
}

type WithCORS struct {
	handler http.Handler
}

// Simple wrapper to Allow CORS.
// See: http://stackoverflow.com/a/24818638/1058612.
func (wrapper *WithCORS) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if origin := req.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}

	// Stop here for a Preflighted OPTIONS request.
	if req.Method == "OPTIONS" {
		return
	}
	// Handler works
	wrapper.handler.ServeHTTP(w, req)
}
