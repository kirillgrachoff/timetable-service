package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Server struct {
	db Database
}

func NewServer(db Database) *Server {
	return &Server{db}
}

func (server *Server) ListAll() ([]Event, error) {
	ans, err := server.db.Get()
	if err != nil {
		return nil, err
	}
	return ans, nil
}

type IncomeTask struct {
	Begin int
	End   int
	Name  string
}

func (server *Server) Add(task IncomeTask) (uint64, error) {
	return server.db.Insert(task)
}

func (server *Server) HandlerAdd(w http.ResponseWriter, req *http.Request) {
	log.Println("HandlerAdd")

	var err error

	contentType := req.Header.Get("Content-Type")
	_ = contentType

	decoder := json.NewDecoder(req.Body)
	decoder.DisallowUnknownFields()

	var task IncomeTask
	if err = decoder.Decode(&task); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	id, err := server.Add(task)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	data, err := json.Marshal(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		panic(err)
	}
}

func (server *Server) HandlerList(w http.ResponseWriter, req *http.Request) {
	log.Println("HandlerList")

	var err error
	ans, err := server.ListAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	jsonData, err := json.Marshal(ans)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jsonData)
	if err != nil {
		panic(err)
	}
}
