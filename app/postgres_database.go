package main

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

const (
	QueryInsertItem         = `INSERT INTO seminars (Begin, "End", Name) VALUES ($1, $2, $3)`
	QueryGetRange           = `SELECT * FROM seminars WHERE Begin <= $1 OR "End" >= $2`
	QueryGetItem            = `SELECT * FROM seminars WHERE Id = $1`
	QueryGetAll             = `SELECT * FROM seminars`
	QueryInsertItemReturnId = `INSERT INTO seminars (Begin, "End", Name) VALUES ($1, $2, $3) Returning Id`
)

type PostgresDatabase struct {
	db *sql.DB
}

func NewPostgresDatabase(dbAddress string) (Database, error) {
	db := &PostgresDatabase{}
	err := db.Open(dbAddress)
	return db, err
}

func (db *PostgresDatabase) Open(dbAddress string) error {
	postgresDb, err := sql.Open("postgres", dbAddress)
	if err != nil {
		return err
	}
	db.db = postgresDb
	return nil
}

func (db *PostgresDatabase) Insert(event IncomeTask) (id uint64, err error) {
	req, err := db.db.Prepare(QueryInsertItemReturnId)
	if err != nil {
		return
	}
	defer req.Close()
	err = req.QueryRow(event.Begin, event.End, event.Name).Scan(&id)
	return
}

func (db *PostgresDatabase) GetRange(from, to string) ([]Event, error) {
	req, err := db.db.Prepare(QueryGetRange)
	if err != nil {
		return nil, err
	}
	defer req.Close()
	rows, err := req.Query(from, to)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	ans := make([]Event, 0, 10)
	for rows.Next() {
		ev := Event{}
		rows.Scan(&ev.Id, &ev.Begin, &ev.End, &ev.Name)
		ans = append(ans, ev)
	}
	return ans, nil
}

func (db *PostgresDatabase) GetItem(id int) (ev Event, err error) {
	req, err := db.db.Prepare(QueryGetItem)
	if err != nil {
		return Event{}, err
	}
	defer req.Close()
	err = req.QueryRow(id).Scan(&ev.Id, &ev.Begin, &ev.End, &ev.Name)
	if err != nil {
		return Event{}, err
	}
	return
}

func (db *PostgresDatabase) Get() ([]Event, error) {
	req, err := db.db.Prepare(QueryGetAll)
	if err != nil {
		return nil, err
	}
	defer req.Close()
	rows, err := req.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	ans := make([]Event, 0, 10)
	for rows.Next() {
		var ev Event
		err = rows.Scan(&ev.Id, &ev.Begin, &ev.End, &ev.Name)
		if err != nil {
			log.Println(err)
		}
		ans = append(ans, ev)
	}
	return ans, nil
}

func (db *PostgresDatabase) Close() error {
	return db.db.Close()
}
