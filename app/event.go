package main

type Event struct {
	Id    uint64
	Begin int
	End   int
	Name  string
}
