package main

type Database interface {
	Open(dbAddress string) error
	Insert(event IncomeTask) (uint64, error)
	Get() ([]Event, error)
	GetRange(from, to string) ([]Event, error)
	GetItem(id int) (Event, error)
	Close() error
}
